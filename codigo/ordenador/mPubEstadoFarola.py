#publicador encargado de cambiar el estado de la farola a on u off si esta en estado manual

import paho.mqtt.publish as publish
import json
import datetime
import os

entrada = ""
while entrada != "on" and entrada != "off":
    entrada = input("Farola on u off: ")
    if entrada != "on" and entrada != "off":
        print("debes escribir on u off")

payload= {
  "modo": entrada,
}

mensaje_pub= json.dumps(payload) #Este es el mensaje a publicar

publish.single(topic="estadoFarola", payload=mensaje_pub, qos=1, hostname="192.168.0.50",keepalive=60)

print("Cambiado el estado de la farola") 