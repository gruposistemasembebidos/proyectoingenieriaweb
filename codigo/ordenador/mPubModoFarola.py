#publicador encargado de cambiar el modo de la farola a manual o automatico 

import paho.mqtt.publish as publish
import json
import datetime
import os

entrada = ""
while entrada != "manual" and entrada != "auto":
    entrada = input("Elige modo manual o auto: ")
    if entrada != "manual" and entrada != "auto":
        print("debes escribir manual o auto")

payload= {
  "modo": entrada,
}

mensaje_pub= json.dumps(payload) #Este es el mensaje a publicar

publish.single(topic="modoFarola", payload=mensaje_pub, qos=1, hostname="192.168.0.50",keepalive=60)

print("Cambiado el modo de la farola") 