#subscriptor encargado de recibir los datos del sensor de luz y guardarlos en un documento
 
import paho.mqtt.client as mqtt
import json



 #-----------------------------------------------
# Callback que se llama cuando el cliente recibe el CONNACK del servidor 
#Restult code 0 significa conexion sin errores
def on_connect(client, userdata, flags, rc):
    print("Conectado con codigo de respuesta  "+str(rc))
    print("Cliente "+str(client))
    print(userdata)

    # Nos subscribirmos al topic 
    client.subscribe(topic)

 #-----------------------------------------------
# Callback que se llama "automaticamente" cuando se recibe un mensaje del Publiser.
def on_message(client, userdata, msg):
    msg.payload = msg.payload.decode("utf-8")
    mensaje_recibido = msg.payload
    print("Cliente "+str(client))
    print("Topic "+str(msg.topic))
   
        
    print("Mensaje recibido "+mensaje_recibido)

    # Obtener los valores a partir de JSON
    mensaje_recibido_json =json.loads(msg.payload )

    varx=mensaje_recibido_json["valorLuz"]
    print("ValorLuz: ",varx)
    vary=mensaje_recibido_json["fecha"]
    print("fecha ",vary)
    #crear un documento para ir guardando los datos del sensor
    with open("documentos/DatosSensor.txt","a") as file:
        file.write(f"{varx} ------- {vary}\n")

    

 #-----------------------------------------------


topic = "valoresFarola"


# Creamos un cliente MQTT 
client = mqtt.Client()   

#Definimos los callbacks para conectarnos y subscribirnos al topic
client.on_connect = on_connect
client.on_message = on_message

puerto = 1883
keepalive = 60
hostname = "192.168.0.50"

# Nos conectamos al broker y mantemos la conexión (loop forever)
client.connect(hostname, puerto, keepalive)
client.loop_forever()