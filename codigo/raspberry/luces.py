#archvio principal encargado de la gestion de la farola

import threading
import RPi.GPIO as GPIO
import time
from datetime import datetime
import sensor_luzS_tsl2561
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import json
import mSubRaspberry

import sys

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

maxLuz = 150
valorPWM = 0

led = 12
GPIO.setup(led, GPIO.OUT)
farola = GPIO.PWM(led, 1000)
farola.start(valorPWM)

funcionando = True
modo = "auto"  # 1=manual y 2=auto
estado = "off"

# funcion para llamar al codigo de subscriptor con difererentes topic
def subscriptorRaspberry(topic):
    print(topic)
    mSubRaspberry.setTopic(topic)

    # Creamos un cliente MQTT
    client = mqtt.Client()

    # Definimos los callbacks para conectarnos y subscribirnos al topic
    client.on_connect = mSubRaspberry.on_connect
    client.on_message = mSubRaspberry.on_message
    puerto = 1883
    keepalive = 60
    hostname = "192.168.0.50"

    # Nos conectamos al broker y mantemos la conexión (loop forever)
    client.connect(hostname, puerto, keepalive)
    client.loop_forever()

#funcion para ajustar el valor PWM de la farola en funcion de la intensidad de luz recibida
def auto(luzVisible):
    if luzVisible < maxLuz:
        valorPWM = (luzVisible * 100)/maxLuz
        valorPWM = 100 - valorPWM
    else:
        valorPWM = 0
    farola.ChangeDutyCycle(valorPWM)
    print("El valor de PWM: ", valorPWM)

#funcion para encender o apagar la farola de forma manual desde el ordenador controlador
def manual():
    #creando el hilo que estará escuchando de fondo la orden de on/off de la farola
    subsEstadoFarola = threading.Thread(
            target=subscriptorRaspberry, name="subsEstadoFarola", daemon=True, args=("estadoFarola",))
    subsEstadoFarola.start()
    global modo
    while modo=="manual":
        # cambiar el modo segun lo recibido por el ordenador
        if mSubRaspberry.getMessage():
            if mSubRaspberry.getMessage() == "manual" or mSubRaspberry.getMessage() == "auto":
                modo = mSubRaspberry.getMessage()
            elif mSubRaspberry.getMessage() == "on" or mSubRaspberry.getMessage() == "off":
                global estado
                estado = mSubRaspberry.getMessage()
        time.sleep(1)
        if estado=="on":
            farola.ChangeDutyCycle(100)
        elif estado=="off":
            farola.ChangeDutyCycle(0)
            
        print (modo ," ", estado)

#cada 10s se enviará al ordenador controlador los datos del sensor de luz y su fecha de recogida
def enviarMQTT():
    #hilo para llamarse a si mismo cada 10s
    threading.Timer(10.0, enviarMQTT).start()
    current_dateTime = datetime.now()
    luzVisible = sensorFarola.read_espectro_visible()
    payload= {
        "valorLuz": str(luzVisible),
        "fecha": str(current_dateTime)
    }
    mensaje_pub= json.dumps(payload) #Este es el mensaje a publicar

    global modo
    #solo mandamos los datos si esta en modo autmatico
    if modo == "auto":
        publish.single(topic="valoresFarola", payload=mensaje_pub, qos=1, hostname="192.168.0.50",keepalive=60, retain=True)
        print("Publicacion OK") 

    
#hilo principal de la aplicacion
if __name__ == "__main__":
    try:
        # sensor de luz de la farola
        sensorFarola = sensor_luzS_tsl2561.SensorLuz()

        # ejecutar programa subscriptor en un hilo aparte. Con Daemon hacemos que se cierre el hilo cuando el hilo main acaba
        subsModoFarola = threading.Thread(
            target=subscriptorRaspberry, name="subsModoFarola", daemon=True, args=("modoFarola",))
        subsModoFarola.start()

        enviarMQTT()
        # ejecutar continuamente hasta que la hora sea > que las 6:00
        while funcionando:
            luzVisible = sensorFarola.read_espectro_visible()
            ahora = datetime.now()
            print("Luz Visible: ", luzVisible)
            time.sleep(1)
            # Tenemos en cuenta que el programa se apagara de 6am a 18pm
            if ahora.hour >= 6 or ahora.hour <= 18:
                funcionando = False
            print(modo)
            # cambiar el modo segun lo recibido por el ordenador
            if mSubRaspberry.getMessage():
                if mSubRaspberry.getMessage() == "manual" or mSubRaspberry.getMessage() == "auto":
                    modo = mSubRaspberry.getMessage()
                elif mSubRaspberry.getMessage() == "on" or mSubRaspberry.getMessage() == "off":
                    estado = mSubRaspberry.getMessage()

            if modo == "auto":
                auto(luzVisible)
            if modo == "manual":
                manual()
        # cerrar programa
        print("Apagando farola")
        sys.exit()

    except KeyboardInterrupt:
        print("Apagando farola")
