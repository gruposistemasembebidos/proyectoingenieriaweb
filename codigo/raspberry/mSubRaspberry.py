#subscriptor de la raspberry encargado de recivir ya se ael modo de la faroloa o el estado de la farola

import paho.mqtt.client as mqtt
import json

#recibir el topic a usar
def setTopic(topic):
    global topicGlobal
    topicGlobal = topic

mensaje=""

#enviar el mensaje recibido
def getMessage():
    global mensaje
    return mensaje

#-----------------------------------------------
# Callback que se llama cuando el cliente recibe el CONNACK del servidor 
#Restult code 0 significa conexion sin errores
def on_connect(client, userdata, flags, rc):
    print("Conectado con codigo de respuesta  "+str(rc))
    print("Cliente "+str(client))
    print(userdata)

    # Nos subscribirmos al topic 
    client.subscribe(topicGlobal)

 #-----------------------------------------------
# Callback que se llama "automaticamente" cuando se recibe un mensaje del Publiser.
def on_message(client, userdata, msg):
    msg.payload = msg.payload.decode("utf-8")
    mensaje_recibido = msg.payload
    print("Cliente "+str(client))
    print("Topic "+str(msg.topic))
   
        
    print("Mensaje recibido "+mensaje_recibido)

    # Obtener los valores a partir de JSON
    mensaje_recibido_json =json.loads(msg.payload )
    global mensaje
    mensaje=mensaje_recibido_json["modo"]
    #print("modo: ",varx)

    

 #-----------------------------------------------
